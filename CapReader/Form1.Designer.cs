﻿namespace CapReader
{
    partial class fmMain
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btStart = new System.Windows.Forms.Button();
            this.comboBox_Com = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label_CH1Value = new System.Windows.Forms.Label();
            this.label_CH0value = new System.Windows.Forms.Label();
            this.label_Ch1 = new System.Windows.Forms.Label();
            this.label_Ch0 = new System.Windows.Forms.Label();
            this.timGetData = new System.Windows.Forms.Timer(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.text_offset = new System.Windows.Forms.TextBox();
            this.labet_ch1_offset = new System.Windows.Forms.Label();
            this.labet_ch0_offset = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label_waterposition = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label_ch1_ave = new System.Windows.Forms.Label();
            this.label_ch0_ave = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btStart);
            this.groupBox1.Controls.Add(this.comboBox_Com);
            this.groupBox1.Location = new System.Drawing.Point(21, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(226, 106);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "COMポート選択";
            // 
            // btStart
            // 
            this.btStart.Location = new System.Drawing.Point(27, 71);
            this.btStart.Name = "btStart";
            this.btStart.Size = new System.Drawing.Size(178, 23);
            this.btStart.TabIndex = 1;
            this.btStart.Text = "開始";
            this.btStart.UseVisualStyleBackColor = true;
            this.btStart.Click += new System.EventHandler(this.btStart_Click);
            // 
            // comboBox_Com
            // 
            this.comboBox_Com.FormattingEnabled = true;
            this.comboBox_Com.Location = new System.Drawing.Point(27, 36);
            this.comboBox_Com.Name = "comboBox_Com";
            this.comboBox_Com.Size = new System.Drawing.Size(178, 20);
            this.comboBox_Com.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label_CH1Value);
            this.groupBox2.Controls.Add(this.label_CH0value);
            this.groupBox2.Controls.Add(this.label_Ch1);
            this.groupBox2.Controls.Add(this.label_Ch0);
            this.groupBox2.Location = new System.Drawing.Point(21, 146);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(226, 142);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "生の測定値";
            // 
            // label_CH1Value
            // 
            this.label_CH1Value.AutoSize = true;
            this.label_CH1Value.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_CH1Value.Location = new System.Drawing.Point(54, 74);
            this.label_CH1Value.Name = "label_CH1Value";
            this.label_CH1Value.Size = new System.Drawing.Size(114, 24);
            this.label_CH1Value.TabIndex = 3;
            this.label_CH1Value.Text = "00000000";
            // 
            // label_CH0value
            // 
            this.label_CH0value.AutoSize = true;
            this.label_CH0value.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_CH0value.Location = new System.Drawing.Point(54, 30);
            this.label_CH0value.Name = "label_CH0value";
            this.label_CH0value.Size = new System.Drawing.Size(114, 24);
            this.label_CH0value.TabIndex = 2;
            this.label_CH0value.Text = "00000000";
            // 
            // label_Ch1
            // 
            this.label_Ch1.AutoSize = true;
            this.label_Ch1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_Ch1.Location = new System.Drawing.Point(6, 85);
            this.label_Ch1.Name = "label_Ch1";
            this.label_Ch1.Size = new System.Drawing.Size(30, 12);
            this.label_Ch1.TabIndex = 1;
            this.label_Ch1.Text = "CH1";
            // 
            // label_Ch0
            // 
            this.label_Ch0.AutoSize = true;
            this.label_Ch0.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_Ch0.Location = new System.Drawing.Point(7, 42);
            this.label_Ch0.Name = "label_Ch0";
            this.label_Ch0.Size = new System.Drawing.Size(30, 12);
            this.label_Ch0.TabIndex = 0;
            this.label_Ch0.Text = "CH0";
            // 
            // timGetData
            // 
            this.timGetData.Tick += new System.EventHandler(this.timGetData_Tick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.text_offset);
            this.groupBox3.Controls.Add(this.labet_ch1_offset);
            this.groupBox3.Controls.Add(this.labet_ch0_offset);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(287, 146);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(218, 142);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "手動マニュアルオフセット";
            // 
            // text_offset
            // 
            this.text_offset.Location = new System.Drawing.Point(58, 19);
            this.text_offset.Name = "text_offset";
            this.text_offset.Size = new System.Drawing.Size(100, 19);
            this.text_offset.TabIndex = 4;
            this.text_offset.Text = "0";
            this.text_offset.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labet_ch1_offset
            // 
            this.labet_ch1_offset.AutoSize = true;
            this.labet_ch1_offset.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labet_ch1_offset.Location = new System.Drawing.Point(54, 105);
            this.labet_ch1_offset.Name = "labet_ch1_offset";
            this.labet_ch1_offset.Size = new System.Drawing.Size(114, 24);
            this.labet_ch1_offset.TabIndex = 3;
            this.labet_ch1_offset.Text = "00000000";
            // 
            // labet_ch0_offset
            // 
            this.labet_ch0_offset.AutoSize = true;
            this.labet_ch0_offset.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.labet_ch0_offset.Location = new System.Drawing.Point(54, 70);
            this.labet_ch0_offset.Name = "labet_ch0_offset";
            this.labet_ch0_offset.Size = new System.Drawing.Size(114, 24);
            this.labet_ch0_offset.TabIndex = 2;
            this.labet_ch0_offset.Text = "00000000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(7, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "CH1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(6, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 12);
            this.label4.TabIndex = 0;
            this.label4.Text = "CH0";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label_waterposition);
            this.groupBox4.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.groupBox4.Location = new System.Drawing.Point(287, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(225, 107);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "2リットルペットボトルの水の高さ(CH0)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(160, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "cm";
            // 
            // label_waterposition
            // 
            this.label_waterposition.AutoSize = true;
            this.label_waterposition.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_waterposition.Location = new System.Drawing.Point(135, 43);
            this.label_waterposition.Name = "label_waterposition";
            this.label_waterposition.Size = new System.Drawing.Size(23, 24);
            this.label_waterposition.TabIndex = 3;
            this.label_waterposition.Text = "0";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label_ch1_ave);
            this.groupBox5.Controls.Add(this.label_ch0_ave);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Location = new System.Drawing.Point(21, 310);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(226, 130);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "10回測定平均(移動平均)";
            // 
            // label_ch1_ave
            // 
            this.label_ch1_ave.AutoSize = true;
            this.label_ch1_ave.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_ch1_ave.Location = new System.Drawing.Point(72, 74);
            this.label_ch1_ave.Name = "label_ch1_ave";
            this.label_ch1_ave.Size = new System.Drawing.Size(114, 24);
            this.label_ch1_ave.TabIndex = 7;
            this.label_ch1_ave.Text = "00000000";
            // 
            // label_ch0_ave
            // 
            this.label_ch0_ave.AutoSize = true;
            this.label_ch0_ave.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_ch0_ave.Location = new System.Drawing.Point(72, 30);
            this.label_ch0_ave.Name = "label_ch0_ave";
            this.label_ch0_ave.Size = new System.Drawing.Size(114, 24);
            this.label_ch0_ave.TabIndex = 6;
            this.label_ch0_ave.Text = "00000000";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(24, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(30, 12);
            this.label7.TabIndex = 5;
            this.label7.Text = "CH1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(25, 42);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 12);
            this.label8.TabIndex = 4;
            this.label8.Text = "CH0";
            // 
            // fmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 471);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "fmMain";
            this.Text = "CapReader";
            this.Load += new System.EventHandler(this.fmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBox_Com;
        private System.Windows.Forms.Button btStart;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label_CH1Value;
        private System.Windows.Forms.Label label_CH0value;
        private System.Windows.Forms.Label label_Ch1;
        private System.Windows.Forms.Label label_Ch0;
        private System.Windows.Forms.Timer timGetData;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox text_offset;
        private System.Windows.Forms.Label labet_ch1_offset;
        private System.Windows.Forms.Label labet_ch0_offset;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_waterposition;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label_ch1_ave;
        private System.Windows.Forms.Label label_ch0_ave;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}

