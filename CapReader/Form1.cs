﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace CapReader
{
    public partial class fmMain : Form
    {

        static SerialPort sp;
        long[] DataBuffer_ch0;
        long[] DataBuffer_ch1;
        int DataCounter;
        bool flag_buffer_full;

        public fmMain()
        {
            InitializeComponent();

            // List ComPort and Select 1st item
            pmListCom();
            if (comboBox_Com.Items.Count >= 1)
            {
                comboBox_Com.SelectedIndex=0;
            }

            // Initialize Data Buffer&Counter
            DataBuffer_ch0 = new long[10];
            DataBuffer_ch1 = new long[10];
            DataCounter = 0;
            flag_buffer_full = false;
        }

        private void fmMain_Load(object sender, EventArgs e)
        {
            
        }

        private void pmListCom()
        {
            string[] ports = SerialPort.GetPortNames();
            foreach (string port in ports)
            {
                comboBox_Com.Items.Add(port);
            }
        }

        private void btStart_Click(object sender, EventArgs e)
        {

            if (btStart.Text == "開始")
            {
                try
                {
                    sp = new SerialPort();
                    sp.PortName = comboBox_Com.SelectedItem.ToString();
                    sp.BaudRate = 115200;
                    sp.DataBits = 8;
                    sp.Parity = Parity.None;
                    sp.StopBits = StopBits.One;
                    sp.Encoding = Encoding.ASCII;

                    sp.Open();
                    btStart.Text = "停止";
                    timGetData.Start();
                }
                catch
                {
                    sp.Close();
                    sp.Dispose();
                    timGetData.Stop();
                    btStart.Text = "開始";
                }
            }else
            {
                timGetData.Stop();
                sp.Close();
                sp.Dispose();
                btStart.Text = "開始";
            }
        }

        private void timGetData_Tick(object sender, EventArgs e)
        {
            try
            {
                string spdata = sp.ReadExisting();
                long[] ch_data = SplitRawdata(spdata);

                if (ch_data[0] != 0)
                {
                    // update - raw data
                    label_CH0value.Text = Convert.ToString(ch_data[0]);
                    label_CH1Value.Text = Convert.ToString(ch_data[1]);

                    label_CH0value.Update();
                    label_CH1Value.Update();

                    // update - offset data
                    if (text_offset.Text != "")
                    {
                        labet_ch0_offset.Text = Convert.ToString(ch_data[0] - Convert.ToInt64(text_offset.Text));
                        labet_ch1_offset.Text = Convert.ToString(ch_data[1] - Convert.ToInt64(text_offset.Text));
                    }

                    labet_ch0_offset.Update();
                    labet_ch1_offset.Update();

                    // Buffer process
                    DataBuffer_ch0[DataCounter] = ch_data[0];
                    DataBuffer_ch1[DataCounter++] = ch_data[1];

                    if (DataCounter >= 10)
                    {
                        DataCounter = 0;
                        flag_buffer_full = true;
                    }

                    label_ch0_ave.Text = Convert.ToString(ave_ch(0));
                    label_ch1_ave.Text = Convert.ToString(ave_ch(1));
                    label_ch0_ave.Update();
                    label_ch1_ave.Update();

                    // Cal WaterPosition
                    if (flag_buffer_full == true)
                    {
                        double temp_sensor = Convert.ToDouble(label_ch0_ave.Text) / 100000 - 220;
                        double wp1 = -1 * Math.Pow(temp_sensor, 3) / 10000 + 0.0119 * Math.Pow(temp_sensor, 2) - 0.7257 * temp_sensor + 29.041;
                        label_waterposition.Text = wp1.ToString("F2");
                    }
                }
            }
            catch
            {
                // 何かしらの理由によって、シリアルポートが切断された場合
                timGetData.Stop();
                sp.Close();
                sp.Dispose();
                MessageBox.Show("シリアルポートが切断されました。\n再接続してください");
                btStart.Text = "開始";
            }
        }
        private long ave_ch(int ch)
        {
            long ave_data_ch = 0;

            if (ch == 0){
                for (int i = 0; i < 10; i++)
                {
                    ave_data_ch += DataBuffer_ch0[i];
                }
            }else{
                for (int i = 0; i < 10; i++)
                {
                    ave_data_ch += DataBuffer_ch1[i];
                }
            }
            ave_data_ch = ave_data_ch / 10;

            return ave_data_ch;
        }
        private long[] SplitRawdata(string str)
        {
            string rawdata_ch0 = "";
            string rawdata_ch1 = "";
            long[] ch = new long[2];

            if (str.IndexOf("Receive Data(") >= 0)
            {
                str = str.Substring(str.IndexOf("Receive Data("));
                try
                {
                    rawdata_ch0 = str.Substring(13, 8);
                    rawdata_ch1 = str.Substring(22, 8);
                }catch
                {
                    rawdata_ch0 = "0";
                    rawdata_ch1 = "0";
                }

                try
                {
                    ch[0] = Convert.ToInt64(rawdata_ch0, 16);
                    ch[1] = Convert.ToInt64(rawdata_ch1, 16);
                }
                catch
                {
                    ch[0] = 0;
                    ch[1] = 0;
                }
            }
            return ch;
        }
        /*
        private void btDebug_Click(object sender, EventArgs e)
        {
            long[] ch = SplitRawdata(textDebug.Text);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        */
    }
}
